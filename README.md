# Les datas en SHS Mires

## Contexte

Dépôt pour le séminaire [Mires Axe 4 – Sciences des données](https://mires.prd.fr/seminaire-axe-4-sciences-des-donnees/)



## Résumé

Tours d'horizon des données en SHS et des infrastructures dédiées à leur "FAIRisation".


## Support de présentation

[![logo diapo](https://gitlab.huma-num.fr/mnauge/logobib/-/raw/master/LR/diaporama.png)](https://gitlab.huma-num.fr/mshs-poitiers/plateforme/les-datas-en-shs-mires/-/blob/main/diaporama/dataShsMires.md)



[Version markdown](https://gitlab.huma-num.fr/mshs-poitiers/plateforme/les-datas-en-shs-mires/-/blob/main/diaporama/dataShsMires.md)



