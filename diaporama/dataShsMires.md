# Datas et SHS

**Tours d'horizon des données en SHS et des infrastructures dédiées à leur "FAIRisation"**

*Michael Nauge, Laboratoire FoReLLIS/MIMMOC, Université de Poitiers*

*David Chesnet, UAR MSHS, Université de Poitiers*


## Disciplines
liste non exhaustive des disciplines présentes à Poitiers
- philosophie
- littérature (génétique textuelle des poèmes latino-américains de prisonniers politiques)
- musicologie
- ethnologie, civilisation, migration, cartographie
- économie
- linguistique de corpus (diachronique prononciation anglaise)
- archéologie
- ...
- sociologie
- psychologie expérimentale (chronométrie mentale, EGG)


[15 laboratoires UP - 3 ED](https://mshs.univ-poitiers.fr/les-laboratoires/)


## Workflow

### Schéma générique de projet en Humanité Numérique (DH)
<img src="https://gitlab.huma-num.fr/mshs-poitiers/forellis/datagrowth/raw/master/schema_GenericProject.svg">


### Données primaires et dérivées
<img src="https://gitlab.huma-num.fr/mshs-poitiers/forellis/datagrowth/raw/master/schema_Primary-Derived.svg">


### Postes autours de la recherche en SHS

- **chercheur** :
Expertise disciplinaire, adresser les questions scientifiques et le pilotage du projet. 
    - [ ] vacataire
    - [x] CDD projet
    - [x] permanent
    - [ ] prestation de service

- **archiviste** : 
Expertise en plan de classement, nommage de fichier, normes/standard métadonnées.
    - [x] vacataire
    - [x] CDD projet
    - [ ] permanent
    - [x] prestation de service

- **ingénieur data/expérimentation** 
Expertise en collecte, traitement, dépôt des données, "FAIRisation".
    - [ ] vacataire
    - [x] CDD projet
    - [x] permanent
    - [ ] prestation de service

- **programmeur** : 
Expertise en développement logicielle, plateforme de diffusion.
    - [ ] vacataire
    - [ ] CDD projet
    - [ ] permanent
    - [x] prestation de service
    
    
## Infrastructures en SHS

### Niveau national : (TG) IR* Huma-Num

[Découvrir Huma-Num](https://www.huma-num.fr/)

**Services numériques**
<img src="https://gitlab.huma-num.fr/mshs-poitiers/plateforme/les-datas-en-shs-mires/raw/main/diaporama/images/ServicesDataHumaNum.png">


[POC callisto Jupyter (Hub)](https://hnlab.huma-num.fr/blog/2021/05/26/callisto-un-demonstrateur-jupyter/)

**Acteur européen**
<img src="https://gitlab.huma-num.fr/mshs-poitiers/plateforme/les-datas-en-shs-mires/raw/main/diaporama/images/europeHumaNum.png">

**Consortiums**
<img src="https://gitlab.huma-num.fr/mshs-poitiers/plateforme/les-datas-en-shs-mires/raw/main/diaporama/images/consortiumsHumaNum.png">



### Niveau établissement Université de Poitiers : MSHS

#### Plateformes technologiques
[Découvrir les plateformes](https://mshs.univ-poitiers.fr/plateformes-technologiques/)

Expertises et équipements techniques et logicielles
- Analyse du comportement humain en temps réel (Cogito)
- Numérisation (Scripto)

<img src="https://gitlab.huma-num.fr/mshs-poitiers/plateforme/les-datas-en-shs-mires/raw/main/diaporama/images/materielPlateformeMshs.jpg">

#### Progedo
[Découvrir la PUD Poitiers](https://mshs.univ-poitiers.fr/plateformes-technologiques/plateforme-universitaire-de-donnees-progedo/)
- Médiation autours des grandes enquêtes (ex. enquête européenne SHARE)
- accès aux données individuelles détaillées et confidentielles (par le CASD – Centre d’Accès Sécurisé aux Données)
- Formation aux approches quantitatives


    
## Conclusion/Objectifs à court 
Fournir aux SHS un service d'accompagnement et de formation à la [Science Ouverte et Reproductible](https://www.ouvrirlascience.fr) : ouverture des articles, **des données** et des codes grâce à une alliance interprofessionnelle d'interlocuteurs de proximités.

- DMP/PGD comme pivot

Si on n'accompagne pas, la ré-utilisation des données est faible.

### Ressources Science Ouverte

- [Guide à destination des doctorants](https://www.ouvrirlascience.fr/wp-content/uploads/2021/10/SO_21-10-14-WEB-FR.pdf)

- [Guide à destination des chercheurs](https://www.ouvrirlascience.fr/wp-content/uploads/2022/04/Guide_Partager_les_donnees_web.pdf)






